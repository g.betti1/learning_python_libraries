import json

class JsonFormatter():
    def __init__(self):
        pass

    def encode(self, arr):
        return json.dumps(arr)

    def save(self, file_path, obj):
        if (type(obj) == str):
            obj = json.loads(str)

        with open(file_path, 'w') as outfile:
            return json.dump(obj, outfile, indent=4)