import mongoengine as db

class User(db.Document):
    name = db.StringField(max_length=200, required=True)
    color = db.StringField(max_length=200, required=False, default="Null")
    age = db.IntField(required=True)

