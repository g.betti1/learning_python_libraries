import mongoengine as db
import certifi
from os import getenv


class Connection:
    # TODO: gestire l'errore con la print
    def __init__(self, alias, database):
        URI = getenv('MONGO_URI')
        if not URI:
            raise NameError("MONGO_URI non è definita come variabile d'ambiente.")
        db.connect(db=database, host=URI, alias=alias, tlsCAFile=certifi.where())
        
    def disconnect(alias):
        db.disconnect(alias)

