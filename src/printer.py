from datetime import datetime

class Printer:
    def __init__(self):
        self.PINK = '\033[95m' #pink
        self.PURPLE = '\033[94m'
        self.OKBLUE = '\033[96m' #blue
        self.OKGREEN = '\033[92m' # green
        self.WARNING = '\033[93m' # yellow
        self.FAIL = '\033[91m' # red
        self.ENDC = '\033[0m'

    def err(self, *args, **kwargs):
        msg = ' '.join(map(str, args))
        print(self.FAIL + msg + self.ENDC, **kwargs)
    
    def warn(self, *args, **kwargs):
        msg = ' '.join(map(str, args))
        print(self.WARNING + self.msg + self.ENDC, **kwargs)

    def pink(self, *args, **kwargs):
        msg = ' '.join(map(str, args))
        print(self.PINK + msg + self.ENDC, **kwargs)

    def purple(self, *args, **kwargs):
        msg = ' '.join(map(str, args))
        print(self.PURPLE + msg + self.ENDC, **kwargs)

    def success(self, *args, **kwargs):
        msg = ' '.join(map(str, args))
        print(self.OKGREEN + msg + self.ENDC, **kwargs)

    def info(self, *args, **kwargs):
            msg = ' '.join(map(str, args))
            print(self.OKBLUE + msg + self.ENDC, **kwargs)