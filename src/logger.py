import logging
from datetime import datetime

class Logger:
    def setup_logger(name, log_file, level=logging.DEBUG):
        """To setup many loggers"""

        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        handler = logging.FileHandler(log_file)  
        handler.setFormatter(formatter)

        logger = logging.getLogger(name)
        logger.setLevel(level)
        logger.addHandler(handler)

        return logger