import argparse

class Parser:
    def __init__(self, description):
        self.parser = argparse.ArgumentParser(description=description)
    
    def arguments(self):
        self.parser.add_argument("-n", "--name", required=True, help="User's name") 
        self.parser.add_argument("-a", "--age", required=True, help="User's age") 
        self.parser.add_argument("-c", "--color", required=False, help="User's favourite color") 

        args = vars(self.parser.parse_args())
        if args["name"].isnumeric() or args["color"].isnumeric() or not args["age"].isnumeric():
            raise RuntimeError("errore nel passaggio dei dati: ricontrollare i tipi di dato")
        return args

    
            