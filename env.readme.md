<p>
Variabili d'ambiente richieste: <br>
MONGO_USERNAME = contiene l'username con cui ci si vuole collegare con mongodb <br>
MONGO_PASSWORD = contiene la password per accedere al database <br>
MONGO_URI = URI per connettersi con all'interno le variabili d'ambiente password e username, che seguono la seguente sintassi: <br>
mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@nome-cluster
</p>
