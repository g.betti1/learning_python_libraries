from dotenv import load_dotenv
load_dotenv()
from src.parser import *
from src.json_formatter import *
from src.db.conf import *
from src.db.user import *
from src.printer import *
from src.logger import *
from logging import DEBUG, ERROR

def main():
    # initializing classes
    logger = logging.getLogger("debug")
    parser = Parser("Informazioni utente")
    printer = Printer()
    json = JsonFormatter()
    conn = Connection("default", "python")

    # getting sys.args
    args = parser.arguments()

    # log information
    printer.pink("Ciao", args["name"])
    logger.info(args["name"] + "ha effettuato l'accesso")

    # working with json files
    json.save("json/data.json", args)
    logger.debug("le informazioni sono state salvate sul json")

    # save json on db
    user = User(name=args["name"], color=args["color"], age=args["age"])
    user.save()
    logger.debug("informazioni salvate sul database")
    conn.disconnect()
    logger.info("disconnessione dal database riuscita")
    logger.debug("uscita dal programma...\n")


if __name__ == "__main__":
    info_log = Logger.setup_logger("debug", "log/debug.log")
    error_log = Logger.setup_logger("error", "log/error.log", ERROR)
    try:
        main()
    except KeyboardInterrupt:
        info_log.debug("uscita forzata dal programma...\n")
    except NameError as e:
        error_log.critical(e)
        info_log.critical(e)

        print(e)
    except RuntimeError as e:
        error_log.critical(e)
        info_log.critical(e)

        print(e)
    except Exception as e:
        info_log.critical(e)
        error_log.critical(e)
        print(e)
