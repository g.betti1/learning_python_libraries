certifi==2021.5.30
cycler==0.10.0
dnspython==1.16.0
inflection==0.2.0
kiwisolver==1.3.1
matplotlib==3.4.2
mongoengine==0.23.1
numpy==1.21.0
opencv-contrib-python==4.5.2.54
opencv-python==4.5.2.54
Pillow==8.3.0
pymongo==3.11.4
pyparsing==2.4.7
python-dateutil==2.8.1
python-dotenv==0.18.0
Schemer==0.2.11
six==1.16.0
